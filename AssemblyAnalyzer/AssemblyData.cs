﻿using System.Reflection;

namespace AssemblyAnalyzer
{
    public class AssemblyData : NamespaceBlob
    {
        internal AssemblyData(Assembly assembly) => Assembly = assembly;

        public Assembly Assembly { get; }
    }
}