﻿using System;
using System.Collections.Generic;

namespace AssemblyAnalyzer
{
    public sealed class NamespaceData : NamespaceBlob
    {
        private readonly HashSet<TypeData> _types = new HashSet<TypeData>();

        public NamespaceData(string name) => Name = name;

        public string Name { get; }

        public IEnumerable<TypeData> Types => _types;

        public bool IsDefinedWithinAssembly { get; internal set; } = true;

        internal TypeData TryInsertType(TypeData typeData)
        {
            if (_types.TryGetValue(typeData, out TypeData foundType))
                return foundType;
            _types.Add(typeData);
            return typeData;
        }

        private bool Equals(NamespaceData other) => Name == other.Name;

        public override bool Equals(object obj) =>
            ReferenceEquals(this, obj) || ((obj is NamespaceData other) && Equals(other));

        public override int GetHashCode() => HashCode.Combine(Name);
    }
}