﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer
{
    public class TypeData : MemberData
    {
        private readonly HashSet<MemberData> _members = new HashSet<MemberData>();

        public TypeData(Type type) : base(type) => Type = type;

        public IEnumerable<MemberData> Members => _members;
        public Type Type { get; }

        public T TryInsertMember<T>(T member) where T : MemberData
        {
            if (_members.TryGetValue(member, out MemberData foundMember))
                return foundMember as T;
            _members.Add(member);
            return member;
        }

        protected override bool IsSpecialName => false;
    }
}