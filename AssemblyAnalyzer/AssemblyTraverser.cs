﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace AssemblyAnalyzer
{
    public class AssemblyTraverser
    {
        private static readonly BindingFlags MemberBindingFlags =
            BindingFlags.Instance |
            BindingFlags.Public |
            BindingFlags.NonPublic |
            BindingFlags.CreateInstance |
            BindingFlags.Static;

        private readonly ICollection<IItemAcceptor> _acceptors = new List<IItemAcceptor>();
        private readonly Assembly _assembly;

        public AssemblyTraverser(Assembly assembly) => _assembly = assembly;

        public void AddItemAcceptor(IItemAcceptor itemAcceptor) => _acceptors.Add(itemAcceptor);

        public void Traverse()
        {
            foreach (TypeInfo typeInfo in _assembly.DefinedTypes)
            {
                if (typeInfo.IsNested)
                    continue;
                TraverseType(typeInfo);
            }
        }

        private void TraverseType(Type type)
        {
            Emit(type);
            IEnumerable<MemberInfo> members =
                type.GetMembers(MemberBindingFlags).Where(member => !IsCompilerGeneratedMember(member));
            foreach (MemberInfo member in members)
            {
                if (member is Type nestedType)
                    TraverseType(nestedType);
                else
                    Emit(member);
            }
        }

        private static bool IsCompilerGeneratedMember(MemberInfo member) =>
            member.IsDefined(typeof(CompilerGeneratedAttribute), false);

        private void Emit<T>(T item)
        {
            foreach (IItemAcceptor acceptor in _acceptors)
            {
                if (!(acceptor is IItemAcceptor<T> filteredAcceptor)) continue;
                filteredAcceptor.Accept(item);
            }
        }
    }
}