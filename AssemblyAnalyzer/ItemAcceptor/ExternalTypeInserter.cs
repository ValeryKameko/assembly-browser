﻿using System.Reflection;

namespace AssemblyAnalyzer.ItemAcceptor
{
    public class ExternalTypeInserter : TypeInserter
    {
        public ExternalTypeInserter(AssemblyData assemblyInfo)
            : base(assemblyInfo, new ExternalNamespaceInserter(assemblyInfo))
        {
        }

        protected override TypeData CreateType(TypeInfo type)
        {
            TypeData typeData = base.CreateType(type);
            typeData.IsDefinedWithinAssembly = false;
            return typeData;
        }
    }
}