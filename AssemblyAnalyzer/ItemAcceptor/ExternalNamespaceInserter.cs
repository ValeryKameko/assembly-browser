﻿using System.Collections.Generic;

namespace AssemblyAnalyzer.ItemAcceptor
{
    public sealed  class ExternalNamespaceInserter : NamespaceInserter
    {
        public ExternalNamespaceInserter(AssemblyData assemblyData) : base(assemblyData)
        {
        }

        protected override NamespaceData CreateNamespacePart(string prefixPart)
        {
            NamespaceData namespacePart = base.CreateNamespacePart(prefixPart);
            namespacePart.IsDefinedWithinAssembly = true;
            return namespacePart;
        }
    }
}