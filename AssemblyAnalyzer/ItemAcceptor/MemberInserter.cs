﻿using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.ItemAcceptor
{
    internal sealed class MemberInserter : IItemAcceptor<MemberInfo>
    {
        private readonly TypeInserter _typeInserter;

        public MemberInserter(AssemblyData assemblyInfo) => _typeInserter = new TypeInserter(assemblyInfo);

        public void Accept(MemberInfo item) => InsertMember(item);

        private MemberData InsertMember(MemberInfo item)
        {
            if (item is TypeInfo typeInfo)
                return _typeInserter.InsertType(typeInfo);
            TypeData outerType = _typeInserter.InsertType(item.ReflectedType.GetTypeInfo());
            if (outerType == null) return null;
            return outerType.TryInsertMember(MemberCreator.CreateMember(item));
        }
    }
}