﻿using System.Reflection;
using System.Runtime.CompilerServices;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.ItemAcceptor
{
    public sealed class ExtensionMethodInserter : IItemAcceptor<MemberInfo>
    {
        private readonly TypeInserter _externalTypeInserter;

        public ExtensionMethodInserter(AssemblyData assemblyData) => _externalTypeInserter = new ExternalTypeInserter(assemblyData);

        public void Accept(MemberInfo item)
        {
            if (!(item is MethodInfo method))
                return;
            if (!item.IsDefined(typeof(ExtensionAttribute), false))
                return;
            TypeInfo injectingType = method.GetParameters()[0].ParameterType.GetTypeInfo();
            TypeData injectingTypeData = _externalTypeInserter.InsertType(injectingType);
            injectingTypeData?.TryInsertMember(new ExtensionMethodData(method));
        }
    }
}