﻿using System.Reflection;
using System.Text.RegularExpressions;

namespace AssemblyAnalyzer.ItemAcceptor
{
    public class TypeInserter : IItemAcceptor<TypeInfo>
    {
        private readonly NamespaceInserter _namespaceInserter;

        public TypeInserter(AssemblyData assemblyInfo) => _namespaceInserter = new NamespaceInserter(assemblyInfo);

        protected TypeInserter(AssemblyData assemblyInfo, NamespaceInserter namespaceInserter) =>
            _namespaceInserter = namespaceInserter;

        public void Accept(TypeInfo item) => InsertType(item);

        public TypeData InsertType(TypeInfo type)
        {
            if (type.IsNested)
            {
                TypeInfo declaringType = type.DeclaringType.GetTypeInfo();
                TypeData outerType = InsertType(declaringType);
                return outerType?.TryInsertMember(CreateType(type));
            }

            if (type.Namespace == null) return null;
            NamespaceData declaringNamespace = _namespaceInserter.InsertNamespace(type.Namespace);
            return declaringNamespace.TryInsertType(CreateType(type));
        }

        protected virtual TypeData CreateType(TypeInfo type) => new TypeData(type);
    }
}