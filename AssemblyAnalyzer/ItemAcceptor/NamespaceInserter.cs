﻿using System.Collections.Generic;
using System.Linq;

namespace AssemblyAnalyzer.ItemAcceptor
{
    public class NamespaceInserter
    {
        private readonly AssemblyData _assemblyData;

        public NamespaceInserter(AssemblyData assemblyData) => _assemblyData = assemblyData;

        public NamespaceData InsertNamespace(string namespaceName) =>
            InsertNamespace(namespaceName.Split('.').Reverse());

        private NamespaceData InsertNamespace(IEnumerable<string> namespaceParts)
        {
            string prefixPart = namespaceParts.First();
            IEnumerable<string> suffixPart = namespaceParts.Skip(1);
            NamespaceBlob outerNamespaceBlob = InsertOuterNamespace(suffixPart);
            return outerNamespaceBlob.TryInsertNamespacePart(CreateNamespacePart(prefixPart));
        }

        protected virtual NamespaceData CreateNamespacePart(string prefixPart) => new NamespaceData(prefixPart);

        private NamespaceBlob InsertOuterNamespace(IEnumerable<string> namespaceParts) =>
            namespaceParts.Any() ? (NamespaceBlob) InsertNamespace(namespaceParts) : _assemblyData;
    }
}