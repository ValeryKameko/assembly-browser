﻿using System;
using System.Collections.Generic;
using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.Formatter
{
    public static class ConstructorFormatter
    {
        public static string Format(ConstructorData constructorData) =>
            Format(constructorData.ConstructorInfo);

        private static string Format(ConstructorInfo constructor)
        {
            string accessModifiers = MethodFormatter.FormatAccessModifiers(constructor);
            string specialModifiers = string.Join(" ", MethodFormatter.CollectSpecialModifiers(constructor));
            Type declaringType = constructor.DeclaringType;
            string genericAttributes = MethodFormatter.FormatGenericParameters(constructor);
            string parameters = MethodFormatter.FormatParameters(constructor);
            return $"{accessModifiers} {specialModifiers} {declaringType.Name}{genericAttributes}{parameters}";
        }
    }
}