﻿using System.Collections.Generic;
using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.Formatter
{
    public static class PropertyFormatter
    {
        public static string Format(PropertyData propertyData) =>
            Format(propertyData.PropertyInfo);

        private static string Format(PropertyInfo property)
        {
            string type = TypeFormatter.FormatInline(property.PropertyType);
            return $"{type} {property.Name}";
        }
    }
}