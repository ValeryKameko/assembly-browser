﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.Formatter
{
    public static class MethodFormatter
    {
        public static string Format(MethodData methodData)
        {
            return Format(methodData.MethodInfo);
        }

        private static string Format(MethodInfo method)
        {
            string accessModifiers = FormatAccessModifiers(method);
            string specialModifiers = string.Join(" ", CollectSpecialModifiers(method));
            string genericParameters = FormatGenericParameters(method);
            string parameters = FormatParameters(method);
            string returnType = TypeFormatter.FormatInline(method.ReturnType);
            return $"{accessModifiers} {specialModifiers} {returnType} {method.Name}{genericParameters}{parameters}";
        }

        internal static string FormatParameters(MethodBase method)
        {
            IEnumerable<string> formattedParameters =
                method.GetParameters()
                    .Select(FormatParameter);
            string parameters = string.Join(", ", formattedParameters);
            return $"({parameters})";
        }

        private static string FormatParameter(ParameterInfo parameter)
        {
            string type = TypeFormatter.FormatInline(parameter.ParameterType);
            string attribute = FormatParameterAttribute(parameter);
            return $"{attribute} {type} {parameter.Name}";
        }

        private static string FormatParameterAttribute(ParameterInfo parameter)
        {
            if (parameter.IsIn && parameter.IsOut)
                return "ref";
            if (parameter.IsIn)
                return "in";
            if (parameter.IsOut)
                return "out";
            return "";
        }

        internal static string FormatGenericParameters(MethodBase method) =>
            method.IsGenericMethod ? TypeFormatter.FormatGenericArguments(method.GetGenericArguments()) : "";

        internal static IEnumerable<string> CollectSpecialModifiers(MethodBase method)
        {
            if (method.IsStatic)
                yield return "static";
            if (method.IsAbstract)
                yield return "abstract";
            if (method.IsFinal)
                yield return "sealed";
            if (method.IsVirtual)
                yield return "virtual";
        }

        internal static string FormatAccessModifiers(MethodBase method)
        {
            if (method.IsPublic)
                return "public";
            if (method.IsFamily)
                return "protected";
            if (method.IsPrivate)
                return "public";
            if (method.IsFamilyOrAssembly)
                return "protected internal";
            if (method.IsFamilyAndAssembly)
                return "private internal";
            if (method.IsAssembly)
                return "internal";
            return null;
        }
    }
}