﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssemblyAnalyzer.Formatter
{
    public static class TypeFormatter
    {
        private static readonly Dictionary<Type, string> FriendlyNames = new Dictionary<Type, string>
        {
            {typeof(object), "object"},
            {typeof(decimal), "decimal"},
            {typeof(bool), "bool"},
            {typeof(char), "char"},
            {typeof(sbyte), "sbyte"},
            {typeof(byte), "byte"},
            {typeof(short), "short"},
            {typeof(ushort), "ushort"},
            {typeof(int), "int"},
            {typeof(uint), "uint"},
            {typeof(long), "long"},
            {typeof(ulong), "ulong"},
            {typeof(float), "float"},
            {typeof(double), "double"},
            {typeof(string), "string"},
            {typeof(void), "void"},
        };

        public static string Format(TypeData typeData) =>
            Format(typeData.Type);

        internal static string FormatInline(Type type)
        {
            string genericAttributes = FormatGenericArguments(type);
            return $"{FormatInlineName(type)}{genericAttributes}";
        }

        private static string FormatInlineName(Type type)
        {
            if (type.IsArray)
                return $"{FormatInlineName(type.GetElementType())}[]";
            if (FriendlyNames.TryGetValue(type, out string friendlyName))
                return friendlyName;
            return FormatName(type);
        }

        private static string Format(Type type)
        {
            string attributes = FormatAttributes(type);
            string genericArguments = FormatGenericArguments(type);
            return $"{attributes} {FormatName(type)}{genericArguments}";
        }

        internal static string FormatGenericArguments(Type[] arguments)
        {
            IEnumerable<string> argumentNames = arguments.Select(FormatInline);
            return $"<{string.Join(", ", argumentNames)}>";
        }

        private static string FormatGenericArguments(Type type) =>
            type.IsGenericType ? FormatGenericArguments(type.GetGenericArguments()) : "";

        private static string FormatName(Type type)
        {
            string name = type.Name;
            return name.Contains('`') ? name.Substring(0, name.IndexOf('`')) : name;
        }

        private static string FormatAttributes(Type type)
        {
            var attributes = new List<string>();
            attributes.Add(FormatAccessModifiers(type));
            attributes.Add(FormatSpecialModifiers(type));
            attributes.Add(FormatTypeAttribute(type));
            IEnumerable<string> nonNullAttributes = attributes.Where(attribute => attribute != null);
            return string.Join(" ", nonNullAttributes);
        }

        private static string FormatTypeAttribute(Type type)
        {
            if (type.IsEnum)
                return "enum";
            if (type.IsValueType)
                return "struct";
            if (type.IsClass)
                return "class";
            if (type.IsInterface)
                return "interface";
            return null;
        }

        private static string FormatSpecialModifiers(Type type)
        {
            if (type.IsAbstract && type.IsSealed)
                return "static";
            if (type.IsAbstract && !type.IsInterface)
                return "abstract";
            if (type.IsSealed && !type.IsEnum)
                return type.IsValueType ? "readonly" : "sealed";
            return null;
        }

        private static string FormatAccessModifiers(Type type)
        {
            if (type.IsPublic || type.IsNestedPublic)
                return "public";
            if (type.IsNestedFamily)
                return "protected";
            if (type.IsNotPublic || type.IsNestedPrivate)
                return "public";
            if (type.IsNestedFamORAssem)
                return "protected internal";
            if (type.IsNestedFamANDAssem)
                return "private internal";
            if (type.IsNestedAssembly)
                return "internal";
            return null;
        }
    }
}