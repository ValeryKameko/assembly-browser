﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.Formatter
{
    public static class FieldFormatter
    {
        public static string Format(FieldData fieldData) =>
            Format(fieldData.FieldInfo);

        private static string Format(FieldInfo field)
        {
            string accessModifiers = FormatAccessModifiers(field);
            string specialModifiers = string.Join(" ", CollectSpecialModifiers(field));
            string formattedType = TypeFormatter.FormatInline(field.FieldType);
            return $"{accessModifiers} {specialModifiers} {formattedType} {field.Name}";
        }
        
        private static IEnumerable<string> CollectSpecialModifiers(FieldInfo field)
        {
            if (field.IsStatic)
                yield return "static";
            if (field.IsInitOnly)
                yield return "readonly";
            if (field.IsLiteral)
                yield return "const";
        }

        private static string FormatAccessModifiers(FieldInfo field)
        {
            if (field.IsPublic)
                return "public";
            if (field.IsFamily)
                return "protected";
            if (field.IsPrivate)
                return "public";
            if (field.IsFamilyOrAssembly)
                return "protected internal";
            if (field.IsFamilyAndAssembly)
                return "private internal";
            if (field.IsAssembly)
                return "internal";
            return null;
        }

    }
}