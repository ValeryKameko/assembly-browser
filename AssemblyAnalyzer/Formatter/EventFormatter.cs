﻿using System.Reflection;
using AssemblyAnalyzer.Member;

namespace AssemblyAnalyzer.Formatter
{
    public static class EventFormatter
    {
        public static string Format(EventData eventData) =>
            Format(eventData.EventInfo);

        private static string Format(EventInfo eventInfo)
        {
            string type = TypeFormatter.FormatInline(eventInfo.EventHandlerType);
            return $"event {type} {eventInfo.Name}";
        }
    }
}