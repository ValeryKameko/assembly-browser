﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssemblyAnalyzer
{
    public class NamespaceBlob
    {
        private readonly HashSet<NamespaceData> _namespaces = new HashSet<NamespaceData>();

        public IEnumerable<NamespaceData> Namespaces => _namespaces;

        public NamespaceData TryInsertNamespacePart(NamespaceData namespacePart)
        {
            if (_namespaces.TryGetValue(namespacePart, out NamespaceData foundNamespacePart))
                return foundNamespacePart;
            _namespaces.Add(namespacePart);
            return namespacePart;
        }
    }
}