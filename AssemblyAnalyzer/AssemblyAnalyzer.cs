﻿using System.Reflection;
using AssemblyAnalyzer.ItemAcceptor;

namespace AssemblyAnalyzer
{
    public class AssemblyAnalyzer
    {
            public AssemblyData Analyze(Assembly assembly)
        {
            var assemblyInfo = new AssemblyData(assembly);
            var assemblyTraverser = new AssemblyTraverser(assembly);
            assemblyTraverser.AddItemAcceptor(new TypeInserter(assemblyInfo));
            assemblyTraverser.AddItemAcceptor(new MemberInserter(assemblyInfo));
            assemblyTraverser.Traverse();

            var secondPassAssemblyTraverser = new AssemblyTraverser(assembly);
            secondPassAssemblyTraverser.AddItemAcceptor(new ExtensionMethodInserter(assemblyInfo));
            secondPassAssemblyTraverser.Traverse();

            return assemblyInfo;
        }
    }
}