﻿namespace AssemblyAnalyzer
{
    public interface IItemAcceptor
    {
    }

    public interface IItemAcceptor<T> : IItemAcceptor
    {
        void Accept(T item);
    }
}