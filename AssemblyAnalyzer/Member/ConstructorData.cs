﻿﻿using System;
using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public sealed class ConstructorData : MemberData
    {
        protected override bool IsSpecialName => false;
        public ConstructorInfo ConstructorInfo { get; }

        public ConstructorData(ConstructorInfo constructorInfo) : base(constructorInfo) => ConstructorInfo = constructorInfo;
    }
}