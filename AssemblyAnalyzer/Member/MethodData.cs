﻿﻿using System.Reflection;
using System.Runtime.CompilerServices;

namespace AssemblyAnalyzer.Member
{
    public class MethodData : MemberData
    {
        public MethodData(MethodInfo methodInfo) : base(methodInfo) => MethodInfo = methodInfo;
        
        public MethodInfo MethodInfo { get; }
        protected override bool IsSpecialName => MethodInfo.IsSpecialName;
    }
}