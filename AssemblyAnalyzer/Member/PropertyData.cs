﻿using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public sealed class PropertyData : MemberData
    {
        internal PropertyData(PropertyInfo propertyInfo) : base(propertyInfo)
        {
            if (propertyInfo.GetMethod != null)
                GetterMethod = new MethodData(propertyInfo.GetMethod);
            if (propertyInfo.SetMethod != null)
                SetterMethod = new MethodData(propertyInfo.SetMethod);
            PropertyInfo = propertyInfo;
        }

        public PropertyInfo PropertyInfo { get; }
        public MethodData GetterMethod { get; }
        public MethodData SetterMethod { get; }
        protected override bool IsSpecialName => false;
    }
}