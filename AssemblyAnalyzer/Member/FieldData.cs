﻿﻿using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public sealed class FieldData : MemberData
    {
        public FieldInfo FieldInfo { get; }

        public FieldData(FieldInfo fieldInfo) : base(fieldInfo) => FieldInfo = fieldInfo;
        
        protected override bool IsSpecialName => FieldInfo.IsSpecialName;
    }
}