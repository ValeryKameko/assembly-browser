﻿using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public sealed class ExtensionMethodData : MethodData
    {
        public ExtensionMethodData(MethodInfo methodInfo) : base(methodInfo)
        {
        }
    }
}