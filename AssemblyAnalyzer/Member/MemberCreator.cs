﻿﻿using System;
using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public static class MemberCreator
    {
        public static MemberData CreateMember(MemberInfo memberInfo)
        {
            switch (memberInfo)
            {
                case ConstructorInfo constructorInfo:
                    return new ConstructorData(constructorInfo);
                case MethodInfo methodInfo:
                    return new MethodData(methodInfo);
                case PropertyInfo propertyInfo:
                    return new PropertyData(propertyInfo);
                case EventInfo eventInfo:
                    return new EventData(eventInfo);
                case FieldInfo fieldInfo:
                    return new FieldData(fieldInfo);
                default:
                    throw new ArgumentException("Invalid member info");
            }
        }
    }
}