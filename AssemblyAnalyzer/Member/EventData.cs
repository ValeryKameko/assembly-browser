﻿using System.Reflection;

namespace AssemblyAnalyzer.Member
{
    public sealed class EventData : MemberData
    {
        public EventData(EventInfo eventInfo) : base(eventInfo)
        {
            EventInfo = eventInfo;
            if (eventInfo.AddMethod != null)
                AddMethod = new MethodData(eventInfo.AddMethod);
            if (eventInfo.RemoveMethod != null)
                RemoveMethod = new MethodData(eventInfo.RemoveMethod);
        }

        public EventInfo EventInfo { get; }
        public MethodData RemoveMethod { get; }
        public MethodData AddMethod { get; }
        protected override bool IsSpecialName => false;
    }
}