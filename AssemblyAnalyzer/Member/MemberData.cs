﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace AssemblyAnalyzer.Member
{
    public abstract class MemberData
    {
        protected MemberData(MemberInfo memberInfo) => MemberInfo = memberInfo;

        public MemberInfo MemberInfo { get; }

        public bool IsDefinedWithinAssembly { get; internal set; } = true;

        public bool IsSpecial =>
            IsSpecialName || MemberInfo.IsDefined(typeof(CompilerGeneratedAttribute), false);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MemberData) obj);
        }

        public override int GetHashCode() => HashCode.Combine(MemberInfo);

        private bool Equals(MemberData other) => MemberInfo == other.MemberInfo;

        protected abstract bool IsSpecialName { get; }
    }
}