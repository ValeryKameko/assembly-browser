﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssemblyAnalyzer.Member;
using FluentAssertions;
using NUnit.Framework;

namespace AssemblyAnalyzer.Test
{
    [TestFixture]
    public class AssemblyAnalyzerTests
    {
        private static readonly string[] MemberNames =
        {
            "NestedClass",
            "CustomEvent",
            "Property",
            ".ctor",
            ".cctor",
            "PublicMethod",
            "ProtectedMethod",
            "PrivateMethod",
            "StaticMethod",
            "ToString",
            "Equals",
            "GetHashCode",
            "GetType",
            "MemberwiseClone",
            "Finalize"
        };

        [Test]
        public void Analyze_ShouldReturnNotNullAssemblyInfo_WhenAssemblyIsEmpty()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.EmptyAssembly);

            assemblyData.Should().NotBeNull();
        }

        [Test]
        public void Analyze_ShouldReturnCorrectNamespaceHierarchy_WhenAssemblyContainsNestedNamespace()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.NestedNamespaceAssembly);

            assemblyData.Namespaces.Should().SatisfyRespectively(
                outerNamespace =>
                {
                    outerNamespace.Name.Should().Be("OuterNamespace");
                    outerNamespace.Namespaces.Should().SatisfyRespectively(
                        nestedNamespace =>
                        {
                            nestedNamespace.Name.Should().Be("NestedNamespace");
                            nestedNamespace.Namespaces.Should().BeEmpty();
                        });
                }
            );
        }

        [Test]
        public void Analyze_ShouldReturnCorrectNamespaceHierarchy_WhenAssemblyContainsNamespaceBranching()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.BranchyNamespaceAssembly);
            NamespaceData outerNamespace = assemblyData.Namespaces.Single();

            outerNamespace.Namespaces.Select(namespaceInfo => namespaceInfo.Name)
                .Should().BeEquivalentTo("LeftNamespace", "RightNamespace");
            outerNamespace.Namespaces.Select(namespaceInfo => namespaceInfo.Namespaces.Count())
                .Should().OnlyContain(namespacesCount => namespacesCount == 0);
        }

        [Test]
        public void Analyze_ShouldCorrectlyPositionClassesInHierarchy_WhenAssemblyContainsMultipleTypes()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.MultipleTypesAssembly);
            NamespaceData outerNamespace = assemblyData.Namespaces.Single();

            outerNamespace.Types.Should().NotBeNull();
            outerNamespace.Types.Select(data => data.MemberInfo.Name).Should().BeEquivalentTo("OuterEnum");

            NamespaceData leftNamespace = outerNamespace.Namespaces.Single(nsname => nsname.Name == "LeftNamespace");
            leftNamespace.Types.Select(data => data.MemberInfo.Name).Should().BeEquivalentTo("FirstClass");

            NamespaceData rightNamespace = outerNamespace.Namespaces.Single(nsname => nsname.Name == "RightNamespace");
            rightNamespace.Types.Select(data => data.MemberInfo.Name).Should().BeEquivalentTo("SecondStruct");
        }

        [Test]
        public void Analyze_ShouldReturnInfoWithCorrectMembers_WhenClassesContainsMembers()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.MethodsAssembly);
            NamespaceData outerNamespace = assemblyData.Namespaces.Single();

            outerNamespace.Types.Single().Members.Should().NotBeNull();
            outerNamespace.Types.Single().Members.Select(memberData => memberData.MemberInfo.Name)
                .Should().BeEquivalentTo(MemberNames);
        }

        [Test]
        public void Analyze_ShouldInsertExtensionMethodsInGivenClasses_WhenClassHierarchyContainsExtensionMethods()
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData assemblyData = analyzer.Analyze(AssemblyCreator.ExtensionsAssembly);
            NamespaceData outerNamespace = assemblyData.Namespaces.Single();
            TypeData injectingClass = outerNamespace.Types.Single();

            injectingClass.Members.Select(member => member.MemberInfo.Name)
                .Should().Contain("ExtensionMethod");
        }
    }
}