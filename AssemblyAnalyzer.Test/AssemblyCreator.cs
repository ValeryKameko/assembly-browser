﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using AssemblyAnalyzer.Test.AssemblySources;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace AssemblyAnalyzer.Test
{
    public static class AssemblyCreator
    {
        public static Assembly NestedNamespaceAssembly =>
            CreateAssembly(new NestedNamespaceAssemblySource());

        public static Assembly EmptyAssembly =>
            CreateAssembly(new EmptyAssemblySource());

        public static Assembly BranchyNamespaceAssembly =>
            CreateAssembly(new BranchyNamespaceAssemblySource());

        public static Assembly MultipleTypesAssembly =>
            CreateAssembly(new MultipleTypesAssembly());
        
        public static Assembly MethodsAssembly =>
            CreateAssembly(new MethodsAssemblySource());

        public static Assembly ExtensionsAssembly =>
            CreateAssembly(new ExtensionsAssemblySource());

        private static readonly MetadataReference[] References =
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
        };

        private static readonly CSharpCompilationOptions CompilationOptions =
            new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary);

        private static Assembly CreateAssembly(AssemblySource source)
        {
            var syntaxTrees = source.Classes.Select(clazz => CSharpSyntaxTree.ParseText(clazz));
            var compilation =
                CSharpCompilation.Create(source.AssemblyName, syntaxTrees, References, CompilationOptions);
            using var stream = new MemoryStream();
            compilation.Emit(stream);
            stream.Seek(0, SeekOrigin.Begin);
            return AssemblyLoadContext.Default.LoadFromStream(stream);
        }
    }
}