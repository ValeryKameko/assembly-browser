﻿using System.Collections.Generic;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public sealed class ExtensionsAssemblySource : AssemblySource
    {
        public override string AssemblyName => "ExtensionsAssembly";
        
        public override IEnumerable<string> Classes => new[]
        {
            @"namespace OuterNamespace {
                public class InjectingClass {
                }
              }",
            @"using OuterNamespace;
            namespace OuterNamespace.InnerClass {
                public static class SecondClass {
                    public static void ExtensionMethod(this InjectingClass instance) {
                    }
                }
              }"
        };
    }
}