﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public sealed class BranchyNamespaceAssemblySource : AssemblySource
    {
        public override string AssemblyName => "BranchyNamespaceAssembly";
        public override IEnumerable<string> Classes => new []
        {
            @"namespace OuterNamespace.LeftNamespace {
                public class FirstClass {
                }
              }",
            @"namespace OuterNamespace.RightNamespace {
                public class SecondClass {
                }
              }"
        };
    }
}