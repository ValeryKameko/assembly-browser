﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public sealed class EmptyAssemblySource : AssemblySource
    {
        public override string AssemblyName => "EmptyAssembly";
        public override IEnumerable<string> Classes => new string[] { };
    }
}