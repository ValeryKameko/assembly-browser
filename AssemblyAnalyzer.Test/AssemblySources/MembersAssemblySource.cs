﻿using System;
using System.Collections.Generic;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public class MethodsAssemblySource : AssemblySource
    {
        public override string AssemblyName => "MethodsAssembly";

        public override IEnumerable<string> Classes => new[]
        {
            @"using System;
            namespace Namespace {
                public class Class {
                    public class NestedClass {
                    }
                    public event EventHandler CustomEvent; 
                    public int Property { get; set; }
                    public Class() { }
                    static Class() { }
                    public void PublicMethod() { }
                    protected void ProtectedMethod() { }
                    private void PrivateMethod() { }
                    public static void StaticMethod() { }
                }
              }"
        };
    }
}