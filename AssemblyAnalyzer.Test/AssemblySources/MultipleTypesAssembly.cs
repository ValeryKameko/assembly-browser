﻿using System.Collections.Generic;
using AssemblyAnalyzer.Test.AssemblySources;

namespace AssemblyAnalyzer.Test
{
    public sealed class MultipleTypesAssembly : AssemblySource
    {
        public override string AssemblyName => "MultipleTypesAssembly";

        public override IEnumerable<string> Classes => new[]
        {
            @"namespace OuterNamespace.LeftNamespace {
                public class FirstClass {
                }
              }",
            @"namespace OuterNamespace.RightNamespace {
                public struct SecondStruct {
                }
              }",
            @"namespace OuterNamespace {
                public enum OuterEnum {
                }
              }"
        };
    }
}