﻿using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public abstract class AssemblySource
    {
        public virtual IEnumerable<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location)
        };

        public abstract string AssemblyName { get; }
        public abstract IEnumerable<string> Classes { get; }
    }
}