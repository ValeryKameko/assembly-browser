﻿using System.Collections.Generic;

namespace AssemblyAnalyzer.Test.AssemblySources
{
    public sealed class NestedNamespaceAssemblySource : AssemblySource
    {
        public override string AssemblyName => "NestedNamespaceAssembly";

        public override IEnumerable<string> Classes => new[]
        {
            @"namespace OuterNamespace.NestedNamespace {
                public class SampleClass {
                }
              }"
        };
    }
}