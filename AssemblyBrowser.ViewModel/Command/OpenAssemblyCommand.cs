﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;

namespace AssemblyBrowser.ViewModel.Command
{
    public sealed class OpenAssemblyCommand : ICommand
    {
        private readonly MainViewModel _mainMainViewModel;

        private static readonly string DialogFilter =
            string.Join("|",
                "Assembly (.dll)|*.dll",
                "All files|*.*");

        private const string DialogTitle = "Open assembly";

        private const string DialogDefaultExt = ".dll";

        public OpenAssemblyCommand(MainViewModel mainViewModel) =>
            _mainMainViewModel = mainViewModel;

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            var openFileDialog = new OpenFileDialog
            {
                DefaultExt = DialogDefaultExt,
                Filter = DialogFilter,
                Title = DialogTitle,
                Multiselect = false,
                CheckFileExists = true,
                CheckPathExists = true
            };
            if (openFileDialog.ShowDialog() != true) return;
            try
            {
                Assembly assembly = Assembly.LoadFrom(openFileDialog.FileName);
                _mainMainViewModel.AnalyzeAssembly(assembly);
            }
            catch (IOException)
            {
                MessageBox.Show("Assembly loading error");
            }
            catch (BadImageFormatException)
            {
                MessageBox.Show("Assembly format is wrong");
            }
        }

        public event EventHandler CanExecuteChanged;
    }
}