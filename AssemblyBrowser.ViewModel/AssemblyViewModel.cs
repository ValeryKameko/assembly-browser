﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using AssemblyAnalyzer;
using AssemblyBrowser.ViewModel.AssemblyItem;

namespace AssemblyBrowser.ViewModel
{
    public sealed class AssemblyViewModel : ViewModelBase
    {
        public AssemblyName AssemblyName => _assemblyData.Assembly.GetName();
        public ObservableCollection<NamespaceViewModel> Namespaces { get; }

        private readonly AssemblyData _assemblyData;

        internal AssemblyViewModel(AssemblyData assemblyData)
        {
            _assemblyData = assemblyData;

            IEnumerable<NamespaceViewModel> namespaceViewModels =
                _assemblyData.Namespaces
                    .Select(namespaceData => new NamespaceViewModel(namespaceData))
                    .Where(nestedNamespace => !nestedNamespace.IsEmpty);
            Namespaces = new ObservableCollection<NamespaceViewModel>(namespaceViewModels);
        }
    }
}