﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AssemblyAnalyzer;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class FlattenedNamespaceViewModel : ViewModelBase
    {
        public ObservableCollection<FlattenedNamespaceViewModel> Namespaces { get; }
        public string Name => string.Join(".", _flattenedNamespaces.Select(namespaceData => namespaceData.Name));

        private IEnumerable<NamespaceData> _flattenedNamespaces;

        internal FlattenedNamespaceViewModel(NamespaceData namespaceData) =>
            _flattenedNamespaces = FlattenNamespace(namespaceData);

        private IEnumerable<NamespaceData> FlattenNamespace(NamespaceData namespaceData)
        {
            var currentNamespace = namespaceData;
            yield return currentNamespace;
            while (!currentNamespace.Types.Any() && (currentNamespace.Namespaces.Count() == 1))
            {
                currentNamespace = currentNamespace.Namespaces.Single();
                yield return currentNamespace;
            }
        }
    }
}