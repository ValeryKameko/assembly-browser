﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using AssemblyAnalyzer;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class NamespaceViewModel : ViewModelBase
    {
        private ObservableCollection<NamespaceViewModel> _namespaces;
        private ObservableCollection<TypeViewModel> _types;
        
        internal NamespaceViewModel(NamespaceData namespaceData)
        {
            IEnumerable<NamespaceViewModel> namespaceViewModels =
                namespaceData.Namespaces
                    .Select(nestedNamespace => new NamespaceViewModel(nestedNamespace))
                    .Where(nestedNamespace => !nestedNamespace.IsEmpty);
            _namespaces = new ObservableCollection<NamespaceViewModel>(namespaceViewModels);

            IEnumerable<TypeViewModel> typeViewModels =
                namespaceData.Types
                    .Where(type => !type.IsSpecial)
                    .Select(type => new TypeViewModel(type));
            _types = new ObservableCollection<TypeViewModel>(typeViewModels);

            InnerItems = new CompositeCollection
            {
                new CollectionContainer {Collection = _types},
                new CollectionContainer {Collection = _namespaces}
            };

            NamespaceName = namespaceData.Name;
        }

        public CompositeCollection InnerItems { get; }
        public string NamespaceName { get; }

        internal bool IsEmpty => 
            (_namespaces.Count == 0) && (_types.Count == 0);
    }
}