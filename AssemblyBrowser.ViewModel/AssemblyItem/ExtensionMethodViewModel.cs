﻿using System.Security.Cryptography.Xml;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class ExtensionMethodViewModel : MethodViewModel
    {
        internal ExtensionMethodViewModel(MethodData methodData)
            : base(methodData)
        {
        }
    }
}