﻿using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class ConstructorViewModel : MemberViewModel
    {
        internal ConstructorViewModel(ConstructorData constructorData) => 
            Signature = ConstructorFormatter.Format(constructorData);

        public string Signature { get; }
    }
}