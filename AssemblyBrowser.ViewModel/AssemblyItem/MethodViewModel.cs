﻿using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class MethodViewModel : MemberViewModel
    {
        internal MethodViewModel(MethodData methodData) =>
            Signature = MethodFormatter.Format(methodData);

        public string Signature { get; }
    }
}