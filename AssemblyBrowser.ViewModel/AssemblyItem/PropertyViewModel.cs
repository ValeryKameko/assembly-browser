﻿using System.Collections.ObjectModel;
using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class PropertyViewModel : MemberViewModel
    {
        internal PropertyViewModel(PropertyData propertyData)
        {
            AccessorMethods = new ObservableCollection<MethodViewModel>();
            if (propertyData.GetterMethod != null)
                AccessorMethods.Add(new MethodViewModel(propertyData.GetterMethod));
            if (propertyData.SetterMethod != null)
                AccessorMethods.Add(new MethodViewModel(propertyData.SetterMethod));
            Signature = PropertyFormatter.Format(propertyData);
        }

        public string Signature { get; }

        public ObservableCollection<MethodViewModel> AccessorMethods { get; }
    }
}