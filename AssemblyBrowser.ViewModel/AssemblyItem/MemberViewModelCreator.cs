﻿using System;
using AssemblyAnalyzer;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public static class MemberViewModelCreator
    {
        public static MemberViewModel CreateMemberViewModel(MemberData memberData)
        {
            switch (memberData)
            {
                case TypeData typeData:
                    return new TypeViewModel(typeData);
                case ExtensionMethodData extensionMethodData:
                    return new ExtensionMethodViewModel(extensionMethodData);
                case MethodData methodData:
                    return new MethodViewModel(methodData);
                case FieldData fieldData:
                    return new FieldViewModel(fieldData);
                case PropertyData propertyData:
                    return new PropertyViewModel(propertyData);
                case ConstructorData constructorData:
                    return new ConstructorViewModel(constructorData);
                case EventData eventData:
                    return new EventViewModel(eventData);
                default:
                    return null;
                    throw new ArgumentException("Invalid member info");
            }
        }
    }
}