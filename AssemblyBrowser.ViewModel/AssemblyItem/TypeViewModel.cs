﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AssemblyAnalyzer;
using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class TypeViewModel : MemberViewModel
    {
        public ObservableCollection<MemberViewModel> Members { get; }

        internal TypeViewModel(TypeData typeData)
        {
            Signature = TypeFormatter.Format(typeData);
            Members = CreateMemberViewModels(typeData.Members);
        }

        public string Signature { get; }
    }
}