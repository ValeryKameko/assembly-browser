﻿using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class FieldViewModel : MemberViewModel
    {
        internal FieldViewModel(FieldData fieldData) => 
            Signature = FieldFormatter.Format(fieldData);

        public string Signature { get; }
    }
}