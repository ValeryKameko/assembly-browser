﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class MemberViewModel : ViewModelBase
    {
        protected MemberViewModel()
        {
        }

        protected static ObservableCollection<MemberViewModel> CreateMemberViewModels(
            IEnumerable<MemberData> typeDataMembers)
        {
            IEnumerable<MemberViewModel> memberViewModels =
                typeDataMembers
                    .Where(member => !member.IsSpecial)
                    .Select(MemberViewModelCreator.CreateMemberViewModel);
            return new ObservableCollection<MemberViewModel>(memberViewModels);
        }
    }
}