﻿using System.Collections.ObjectModel;
using System.Windows.Data;
using AssemblyAnalyzer.Formatter;
using AssemblyAnalyzer.Member;

namespace AssemblyBrowser.ViewModel.AssemblyItem
{
    public class EventViewModel : MemberViewModel
    {
        internal EventViewModel(EventData eventData)
        {
            MethodData addMethod = eventData.AddMethod;
            if (addMethod != null)
                Methods.Add(new MethodViewModel(addMethod));
            MethodData removeMethod = eventData.RemoveMethod;
            if (removeMethod != null)
                Methods.Add(new MethodViewModel(removeMethod));
            
            Signature = EventFormatter.Format(eventData);
        }

        public ObservableCollection<MethodViewModel> Methods { get; } = 
            new ObservableCollection<MethodViewModel>();
        public string Signature { get; }
    }
}