﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using AssemblyAnalyzer;
using AssemblyBrowser.ViewModel.Command;

namespace AssemblyBrowser.ViewModel
{
    using AssemblyAnalyzer = AssemblyAnalyzer.AssemblyAnalyzer;

    public sealed class MainViewModel : ViewModelBase
    {
        private AssemblyViewModel _selectedAssembly;

        public ObservableCollection<AssemblyViewModel> AssemblyList { get; } =
            new ObservableCollection<AssemblyViewModel>();
        public AssemblyViewModel SelectedAssembly
        {
            get => _selectedAssembly;
            set
            {
                _selectedAssembly = value;
                OnPropertyChanged();
            }
        }

        public ICommand OpenAssemblyCommand => new OpenAssemblyCommand(this);

        public ICommand ExitCommand => new ExitCommand();
        
        internal void AnalyzeAssembly(Assembly assembly)
        {
            var analyzer = new AssemblyAnalyzer();
            AssemblyData analyzedAssembly = analyzer.Analyze(assembly);
            var assemblyViewModel = new AssemblyViewModel(analyzedAssembly);
            AssemblyList.Add(assemblyViewModel);
            if (AssemblyList.Count == 1)
                SelectedAssembly = AssemblyList.First();
        }
    }
}